

import java.io.*;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.sql.*;
import java.util.Scanner;

@Retention(RetentionPolicy.RUNTIME)
@interface TestAnno
{

}

interface writelog
{
    File f = new File("C:\\Users\\amar-7488\\Documents\\NetBeansProjects\\BankApplication2\\error_log.txt");
    void updateLog(Exception e) throws IOException;
}

interface Iscan
{
    Scanner scan = new Scanner(System.in);
}


@SuppressWarnings("checked")
abstract class Bank implements Iscan, Runnable, writelog {
    ResultSet getCust() throws SQLException {
        Connection db = DriverManager
                .getConnection("jdbc:postgresql://localhost:5432/mydb",
                        "amar7488", "");
        Statement stmt = db.createStatement();
        System.out.println("Enter accno: ");
        int accno = scan.nextInt();
        String query = "select * from customers where accno = " + accno;
        ResultSet rs = stmt.executeQuery(query);
//        stmt.close();
//        db.close();
        return rs;
    }

    void viewCust() throws SQLException {
        ResultSet rs = getCust();
        rs.next();
        System.out.println("Name: " + rs.getString("name") +
                " Balance: " + rs.getFloat("balance"));

    }

    void transac() throws InterruptedException {
        System.out.println("Transac()");

            Thread t = new Thread(new transaction() {
                @Override
                void reg_transac() throws Exception {

                }

                @Override
                public void run() {
                    try {
                        update_tid();
                    } catch (SQLException e) {
                        e.printStackTrace();
                        updateLog(e);
                    }

                }
            });
            t.start();

        int ch = 0;
        while(ch!=5)
        {
            System.out.println("\n------------------------------------");
            System.out.println("1.View Transactions 2.Debit 3.Credit"
                    + " 4.View Acc Details 5.Main Menu");
            System.out.print("Enter your choice: ");
            ch = scan.nextInt();
            scan.nextLine();
            switch(ch) {
                case 1:
                    System.out.println("View Transaction Details..");
                    Thread viewT = new Thread(new transaction() {
                        @Override
                        void reg_transac() throws Exception {

                        }

                        @Override
                        public void run() {
                            System.out.println("Enter accno: ");
                            accno = scan.nextInt();
                            query = "select * from trans where accno="+accno;
                            try {
                                ResultSet rs = s.executeQuery(query);
                                System.out.println("Tid--AccNo--Balance---Amount---Description");
                                while(rs.next()) {
                                    System.out.println(rs.getInt(1)+"-----"+
                                            rs.getInt(2)+"---"+
                                            rs.getFloat(3)+"---"+
                                            rs.getFloat(4)+"-----"+
                                            rs.getString(5));
                                }
                            } catch (SQLException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                    viewT.start();
                    viewT.join();
                    break;

                case 2:
                    debit d = new debit();
                    Thread debitT = new Thread(d);
                    debitT.start();
                    debitT.join();
                    break;
                case 3:
                    credit c = new credit();
                    Thread creditT = new Thread(c);
                    creditT.start();
                    creditT.join();
                    break;
                case 4:
                    try {
                        viewCust();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                    break;
                case 5:
                    System.out.println("Quitting Transaction menu....");
                    break;
                default:
                    System.out.println("Wrong Choice");
            }
        }
    }

    @TestAnno
    @Override
    protected void finalize() throws Throwable {
        super.finalize();
    }

    void dbupdate(int accno, float amt) {
        try {
            Connection db = DriverManager.getConnection("jdbc:postgresql://localhost:5432/mydb",
                            "amar7488", "");
            Statement stmt = db.createStatement();
            String query = "update customers set balance=balance+"+amt+" where accno="+accno;
            stmt.executeUpdate(query);
            stmt.close();
            db.close();
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("Error in Credit/Debit...!!");
        }
        System.out.println("Update Success...");
    }


    public void updateLog(Exception e) {
        try {
            FileWriter fw = new FileWriter(f);
            fw.write(e.getMessage() + "\n");
            fw.close();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }
}

abstract class transaction extends Bank
{
    static int tid = 0;
    int accno;
    float bal, amt;
    private String db_path = "jdbc:postgresql://localhost:5432/mydb", username = "amar7488", pass = "";
    Connection c;
    {
        try {
            c = DriverManager.getConnection(db_path,username,pass);
            s = c.createStatement();
        } catch (SQLException e) {
            e.printStackTrace();
            updateLog(e);
        }
    }

    ;
    Statement s;
    String query;
    abstract void reg_transac() throws Exception;
    void update_tid() throws SQLException {
        query = "select count(*) from trans";
        ResultSet rs = s.executeQuery(query);
        rs.next();
        tid = rs.getInt(1)+1;
        s.close();
    }
}



class registration extends Bank{


    String name;
    int age;
    static int accNo;
    float balance;

    public void run() {
        try {
            getD();
        } catch (InterruptedException e) {
            e.printStackTrace();
            updateLog(e);
        } catch (SQLException e) {
            e.printStackTrace();
            updateLog(e);
        }
    }

    void getD() throws InterruptedException, SQLException {
        Thread t = new Thread(new Bank() {
            @Override
            public void run() {
                try {
                    findAccno();
                } catch (SQLException e) {
                    e.printStackTrace();
                    updateLog(e);
                }
            }
        });
        t.start();
        System.out.print("Enter name:");
        this.name = scan.nextLine();
        System.out.print("Enter age:");
        this.age = Integer.parseInt(scan.nextLine());
        this.balance = 3000;
        t.join();
//        transaction temp = new transaction();
//        temp.addT("Account Opened",1,5000,0);
//        trans.add(temp);
        insertDB();
//        reg_transac();
    }

    void findAccno() throws SQLException {
        Connection db = DriverManager
                .getConnection("jdbc:postgresql://localhost:5432/mydb",
                        "amar7488", "");
        Statement stmt = db.createStatement();
        String query = "select count(*) from customers";
        ResultSet rs = stmt.executeQuery(query);
        rs.next();
        accNo = rs.getInt(1) + 1;
        System.out.println("RS Int "+accNo);
        stmt.close();
        db.close();
    }

    void insertDB() throws SQLException {
        Connection db = DriverManager
                .getConnection("jdbc:postgresql://localhost:5432/mydb",
                        "amar7488", "");
        Statement stmt = db.createStatement();
        String query = "insert into customers values"
                + "(" + accNo + ",'" + name + "'," +
                + age + "," + balance + ")";
        stmt.executeUpdate(query);
        stmt.close();
    }
}


class debit extends transaction
{

    public void run() {
        ResultSet rs;
        try {
            rs = getCust();
            System.out.println("Enter Debit Amount: ");
            amt = scan.nextFloat();
            rs.next();
            bal = rs.getFloat("balance");
            accno = rs.getInt("accno");
            if (bal - amt < 0)
                System.out.println("Entered amount is greater than available balance");
            else
                dbupdate(accno, -amt);
            reg_transac();
        } catch (SQLException e) {
            e.printStackTrace();
            updateLog(e);
        }
    }

    @Override
    void reg_transac() throws SQLException {
        s = c.createStatement();
        if(amt<=bal)
            query = "insert into trans values"
                    + "(" + tid + ",'" + accno + "'," +
                    + (bal-amt) + "," + amt + ",'Withdraw')";
        else
            query = "insert into trans values"
                    + "(" + tid + ",'" + accno + "'," +
                    + bal + "," + amt + ",'Unsuccess')";
        s.executeUpdate(query);
        s.close();
        tid++;
    }
}

class credit extends transaction
{
    public void run() {
        ResultSet rs;
        try {
            rs = getCust();
            System.out.println("Enter Credit Amount: ");
            amt = scan.nextFloat();
            rs.next();
            bal = rs.getFloat("balance");
            accno = rs.getInt("accno");
            dbupdate(accno, amt);
            reg_transac();
        } catch (SQLException e) {
            e.printStackTrace();
            updateLog(e);
        }
    }

    @Override
    void reg_transac() throws SQLException {
        s = c.createStatement();
        query = "insert into trans values"
                + "(" + tid + ",'" + accno + "'," +
                + (bal+amt) + "," + amt + ",'Deposit')";
        s.executeUpdate(query);
        s.close();
        tid++;
    }
}

public class BankAppWithDB implements Iscan {

    public static  void main(String args[]) throws InterruptedException {
        int choice = 0;
        while(choice!=4) {
            System.out.println("*******************************\n"
                    + "Banking Application");
            System.out.println("1.Register 2.View Details 3.Transaction 4.Exit");
            System.out.print("Enter your choice: ");
            choice = scan.nextInt();
            scan.nextLine();

            switch (choice) {
                case 1:
                    System.out.println("Register");
                    Bank reg = new registration();
                    Thread c1 = new Thread(reg);
                    c1.start();
                    c1.join();
                    System.out.println("******************************\n");
                    break;
                case 2:
                    System.out.println("View D");
                    Thread c2 = new Thread(new Bank() {
                        public void run()
                        {
                            try {
                                viewCust();
                            } catch (SQLException e) {
                                e.printStackTrace();
                                updateLog(e);
                            }
                        }
                    });
                    c2.start();
                    c2.join();
                    System.out.println("******************************\n");
                    break;
                case 3:
                    System.out.println("Trans");
                    Thread c3 = new Thread(new Bank() {
                        public void run()
                        {
                            try {
                                transac();
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                                updateLog(e);
                            }
                        }
                    });
                    c3.start();
                    c3.join();
                    System.out.println("******************************\n");
                    break;
                case 4:
                    System.out.println("Exiting........");
                    System.out.println("******************************\n");
                    break;
                default:
                    System.out.println("Wrong Choice");
                    System.out.println("******************************\n");
            }
        }
    }
}
