/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author Amar-7488
 */
@Retention(RetentionPolicy.RUNTIME)
@interface TestAnno
{
    
}

interface Iscan
{
    Scanner scan = new Scanner(System.in);
}




@SuppressWarnings("checked")
class Bank implements Iscan, Runnable
{
    String bankName;
    int id;
    ArrayList<customer> cust;

    public void run()
    {

    }

    Bank() {
        this.cust = new ArrayList<customer>();
        
        this.predefined();
    }
    
    void init() throws FileNotFoundException, IOException
    {
        System.out.println("Reading Customer File...");
        File f = new File("details.txt");
        BufferedInputStream br = new BufferedInputStream(new FileInputStream(f));
        while(br.available() != 0)
        {
            System.out.print((char)(br.read()));
        }
        br.close();
    }
    @TestAnno
    final void predefined()
    {
        customer temp;
        customer c = new customer();
        temp = c.createCust("aaa",30);
        cust.add(temp);
        temp = c.createCust("bbb",22);
        cust.add(temp);
        temp = c.createCust("ccc",27);
        cust.add(temp);
    }
    void addCust()
    {
        customer c = new customer();
        c.getD();
        cust.add(c);
    }
    
    void viewCust()
    {
        System.out.println("AccNo------Name-----Age");
        for (customer c: cust)
            c.putD();
    }
    
    customer findCust()
    {
        System.out.print("Enter AccNo.: ");
        int accNo = scan.nextInt();
        String pswd = scan.nextLine();
        for(customer c : cust)
        {
            if(c.accNo == accNo)
                return c;
            else
                continue;
        }
        return null;
    }
    
    void transact()
    {
        int ch=0,amt=0,lastId;
        customer temp;
        while(ch!=4)
        {
            System.out.println("\n------------------------------------");
            System.out.println("1.View Transactions 2.Deposit 3.Withdraw"
                    + " 4.Main Menu");
            System.out.print("Enter your choice: ");
            ch = scan.nextInt();
            scan.nextLine();
            switch(ch)
            {
                case 1:
                    temp = findCust();
                    if (temp == null)
                    {
                        System.out.println("Acc No. does not exist");
                        break;
                    }
                    temp.viewTrans();
                    System.out.println("------------------------------------");
                    break;

                case 2:
                    temp = findCust();
                    if (temp == null)
                    {
                        System.out.println("Acc No. does not exist");
                        break;
                    }
                    System.out.print("Enter amount: ");
                    amt = scan.nextInt();
                    
                    transaction t = temp.trans.get(temp.trans.size() - 1);
                    lastId = t.id;
                    t = new transaction();
                    temp.balance += amt;
                    t.addT("Deposit", lastId+1, amt, temp.balance);
                    temp.trans.add(t);
                    System.out.println("------------------------------------");
                    break;

                case 3:
                    temp = findCust();
                    if (temp == null)
                    {
                        System.out.println("Acc No. does not exist");
                        break;
                    }
                    System.out.print("Enter amount: ");
                    amt = scan.nextInt();
                    
                    transaction tt = temp.trans.get(temp.trans.size() - 1);
                    lastId = tt.id;
                    tt = new transaction();
                    temp.balance -= amt;
                    tt.addT("Withdraw", lastId+1, amt, temp.balance);
                    temp.trans.add(tt);
                    System.out.println("------------------------------------");
                    break;

                case 4:
                    System.out.println("------------------------------------\n");
                    break;

                default:
                    System.out.println("Wrong Choice");
                    System.out.println("------------------------------------\n");
            }
        }
    }
    
    void writeFile() throws IOException
    {
        File w = new File("details.txt");
        FileWriter fw = new FileWriter(w);
        BufferedWriter bw = new BufferedWriter(fw);
        
        for (customer c : cust)
        {
            bw.write(c.accNo + " " +
                    c.age + " " +
                    c.balance + " " +
                    c.name);
            bw.newLine();
        }
        bw.close();
        System.out.println("Updating Customer file");
        
    }

}
class customer implements Iscan
{
    static int accNum = 1;
    int accNo,age,balance;
    String name;
    ArrayList<transaction> trans;

    customer() {
        this.trans = new ArrayList<transaction>();
    }
    
    void getD()
    {
        System.out.print("Enter name:");
        this.name = scan.nextLine();
        System.out.print("Enter age:");
        this.age = Integer.parseInt(scan.nextLine());
        this.accNo = accNum;
        accNum++;
        transaction temp = new transaction();
        temp.addT("Account Opened",1,5000,0);
        trans.add(temp);
    }
    
    void putD()
    {
        System.out.println(this.accNo + "---------" +
                this.name + "--------" + this.age +
                "-------" + this.balance);
    }
    
    customer createCust(String name, int age)
    {
        customer t = new customer();
        t.name = name;
        t.age = age;
        t.accNo = accNum;
        t.balance = 5000;
        accNum++;
        transaction temp = new transaction();
        temp.addT("Account Opened",1,5000,0);
        t.trans.add(temp);
        return t;
    }
    
    void viewTrans()
    {
        System.out.println("No----------Desc-----Amt-----Balance");
        for(transaction t : trans)
        {
            t.displayT();
        }
    }
}

class transaction
{
    int id, amt, balance;
    String desc;
    
    void addT(String s, int id, int amt, int bal)
    {
        this.id = id;
        this.amt = amt;
        this.desc = s;
        this.balance = bal;
    }
    void displayT()
    {
        System.out.println(this.id + "-----" +
                this.desc + "---" + this.amt + "-----" + this.balance);
    }
}


public class BankApplication2 implements Iscan,Runnable{
    
    /**
     * @param args the command line arguments
     */

    
    public static void main(String[] args) throws IOException, InterruptedException {
        // TODO code application logic here
                    
        Thread tt = new Thread(new BankApplication2());
        tt.start();
        tt.join();
        System.out.println("Thankyou..");
        

    }

    
    @Override
    public void run() {
        
        System.out.println("Thread Created\n");
        
        int ch=0;
        Bank b = new Bank();
        try {
            b.init();
        } catch (FileNotFoundException ex)
        {
        } catch (IOException ex) 
        {
        }
        while(ch!=4)
        {
            System.out.println("*******************************\n"
                    + "Banking Application");
            System.out.println("1.Register 2.View Details 3.Transaction 4.Exit");
            System.out.print("Enter your choice: ");
            ch = scan.nextInt();
            scan.nextLine();

            switch(ch)
            {
                case 1:
                    System.out.println("Register");
                    b.addCust();
                    System.out.println("******************************\n");
                    break;
                case 2:
                    System.out.println("View D");
                    b.viewCust();
                    System.out.println("******************************\n");
                    break;
                case 3:
                    System.out.println("Trans");
                    b.transact();
                    System.out.println("******************************\n");
                    break;
                case 4:
                    System.out.println("Exiting........");
                    try {
                        b.writeFile();
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                    System.out.println("******************************\n");
                    break;
                default:
                    System.out.println("Wrong Choice");
                    System.out.println("******************************\n");
            }
        }
    }

}
